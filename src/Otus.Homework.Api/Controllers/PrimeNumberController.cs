﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Homework.Api.Controllers
{
    [Route("prime")]
    [Authorize]
    public class PrimeNumberController : Controller
    {
        [HttpGet]
        public IActionResult Get() => Ok(37);
    }
}
